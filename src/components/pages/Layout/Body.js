import React, { Component } from 'react';
import {CircularProgress,Grid,Paper} from "@material-ui/core";
import {MovieList} from "../../widgets/MovieList";
import {connect} from "react-redux";
import {getMovies} from "../../../redux/actions";
import {InfinitScroll} from "../../widgets/InfinitScroll";

class Body extends Component {

    componentDidMount() {
        this.props.getMovies({term:'titanic'})
    }

    render() {
        const {movies} = this.props

        return (
            <Paper elevation={5}>

                <div style={{height:100}} />

                <Grid container>

                    <Grid item xs={1} />
                    <Grid item xs={10} style={{textAlign:'center'}} >

                        {movies ? <Grid container justify={'space-between'} alignItems={'center'} direction={'row'}>

                            <MovieList movies={movies} />

                        </Grid> :<CircularProgress />}

                        <InfinitScroll />

                    </Grid>
                    <Grid item xs={1} />

                </Grid>

            </Paper>
        );
    }
}

export default connect(({API}) => {
    let movies = null
    const {result} = API
    if(result && result.Response === 'True'){
        movies = result.Search
    }
    else{
        movies = [
            {Title: 'Not found, try again', imdbID:'', Poster:''}
        ]
    }

    return {
        movies
    }
}, {getMovies})(Body)
