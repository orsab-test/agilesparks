import React, { Component } from 'react';
import {AppBar, Grid, Toolbar, Typography} from "@material-ui/core";
import {SearchField} from "../../widgets/SearchField";

class Header extends Component {
    render() {
        return (
            <AppBar position="fixed" color="default">
                <Toolbar>


                    <Typography variant="h6" color="inherit">
                        Movies
                    </Typography>

                    <Grid container justify={'center'}>
                        <Grid item>
                            <SearchField />
                        </Grid>
                    </Grid>

                </Toolbar>
            </AppBar>
        );
    }
}

export default Header
