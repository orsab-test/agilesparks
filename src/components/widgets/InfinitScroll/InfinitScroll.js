import React from 'react'

import {connect} from "react-redux";
import {getMovies} from "../../../redux/actions";
import style from './style.css'

class InfinitScroll extends React.PureComponent{

    state = {
        page: null
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    render = () => {

        const {totalResults, page, Error} = this.props

        return (
            (page*10) < totalResults && !Error?
                <div>
                    {page}/{Math.ceil(totalResults/10)}
                    <div className="waves" onClick={this.handleScroll}>
                        <span className="wave">Load more</span>
                        <span className="wave">Load more</span>
                        <span className="wave">Load more</span>
                    </div>
                </div>
                : <div>{page}/{totalResults ? Math.ceil(totalResults/10) : 1}</div>
        )
    }

    handleScroll = e => {
        const top = document.documentElement.scrollTop
        const el = document.getElementsByTagName('html')[0]


        if((el.clientHeight + top) === el.scrollHeight){
            const {totalResults, page, term, Error, getMovies} = this.props

            if(this.state.page === page)
                return

            console.log((el.clientHeight + top) === el.scrollHeight)
            this.setState({page})

            if((page*10) < totalResults && !Error)
                getMovies({term, page: page+1, append: true})
        }
    }

}

export default connect(({API}) => {
    const {result, page, term} = API

    if(!result)
        return {}

    return {
        totalResults: parseInt(result.totalResults),
        page,
        term,
        Error: result.Error,
    }
}, {getMovies})(InfinitScroll)