import React from 'react'

import {Typography, Popover} from "@material-ui/core";
import PropTypes from 'prop-types'
import {connect} from "react-redux";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

class PopoverInfo extends React.PureComponent{

    componentDidMount() {
    }

    render = () => {

        const {element, handleClose, detailData} = this.props
        const open = Boolean(element)

        return <Popover
            id="simple-popper"
            open={open}
            anchorEl={element}
            onClose={handleClose}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
            }}
        >
            <Card >
                <CardContent>
                    {this._renderData(detailData)}
                </CardContent>
            </Card>
        </Popover>

    }

    _renderData = (data) => {
        return data ? Object.keys(data).map((item, key) => {
            if(['Poster','Response','Type'].includes(item))
                return null

            return <div key={key}>
                <span style={{fontWeight:'bold'}}>{item}: </span>
                {Array.isArray(data[item]) ? data[item].map((i,k) => {
                    return <span key={k}>{i.Source}: {i.Value}</span>
                }) : <span style={{}}>{data[item]}</span>}
            </div>
        }) : null
    }

}

PopoverInfo.propTypes = {
    data: PropTypes.object,
}

export default connect(({API}) => {
    const {detailData} = API

    return {
        detailData
    }
}, {})(PopoverInfo)