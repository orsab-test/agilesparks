import React from 'react'
import Card from "@material-ui/core/Card"
import CardActionArea from "@material-ui/core/CardActionArea"
import CardMedia from "@material-ui/core/CardMedia"
import CardContent from "@material-ui/core/CardContent"
import Typography from "@material-ui/core/Typography"

import PropTypes from 'prop-types'
import {PopoverInfo} from "../PopoverInfo";
import {connect} from "react-redux";
import {getMovie} from "../../../redux/actions";

class MovieItem extends React.PureComponent{

    state = {
        element: null,
        id: null
    }


    render = () => {
        const {Poster, Title, Year, imdbID} = this.props
        const {element} = this.state

        return (
            <div>
                <Card elevation={6} style={{margin:5}}>
                    <CardActionArea>
                        {Poster ? <CardMedia
                            onClick={this.handleMoreInfo(imdbID)}
                            image={Poster}
                            title={Title}
                            style={{height:140, backgroundSize:'contain'}}
                        /> : null}
                        <CardContent style={{textAlign:'center'}}>
                            <Typography style={{fontWeight:'bold'}}>
                                {Title}
                            </Typography>
                            <Typography component={'small'}>
                                {Year}
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
                <PopoverInfo element={element} handleClose={this.handleClose} />
            </div>
        )

    }

    handleMoreInfo = id => e => {

        const element = e.currentTarget

        this.props.getMovie({id}).then(()=>{
            this.setState({
                id,
                element,
            });
        })

    }

    handleClose = e => {
        this.setState({
            element: null
        })
    }
}

MovieItem.propTypes = {
    Title: PropTypes.string.isRequired,
    imdbID: PropTypes.string.isRequired,
    Poster: PropTypes.string,
    Year: PropTypes.string,
}

export default connect(({API}) => {
    return {}
}, {getMovie})(MovieItem)