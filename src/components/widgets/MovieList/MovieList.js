import React from 'react'
import {List, ListItem} from "@material-ui/core"
import MovieItem from "./MovieItem"

import PropTypes from 'prop-types'
import Grid from "@material-ui/core/Grid";
import Grow from "@material-ui/core/Grow";

class MovieList extends React.Component{

    render = () => {
        const {movies} = this.props

        return <Grid container direction={'row'} justify={'space-evenly'}>
            {movies.map(({Title, Poster, Year, imdbID}, key) => {
                return (
                    <Grow in={true} timeout={100*key / (movies.length / 10)} key={key}>
                        <Grid item >
                            <MovieItem Title={Title} Poster={Poster} Year={Year} imdbID={imdbID} />
                        </Grid>
                    </Grow>
                )
            })}
            </Grid>

    }
}

MovieList.propTypes = {
    movies: PropTypes.array.isRequired
}

export default MovieList