import React from 'react'

import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import {cleanSearch, getMovies} from "../../../redux/actions";

class SearchField extends React.Component{

    state = {
        term: '',
        isError: false
    }

    render = () => {

        const {term, isError} = this.state

        return <div style={{textAlign:'center'}}>
            <TextField
                id="standard-search"
                label="Search here..."
                value={term}
                onChange={this.handleSearch}
                type="search"
                error={isError}
                margin="normal"
            />
        </div>

    }

    handleSearch = e => {
        const term = e.target.value
        this.setState({term})

        if(term.length < 3) {
            this.setState({isError:true})

            return
        }

        this.setState({isError:false})
        this.props.cleanSearch()
        this.props.getMovies({term})
    }
}

export default connect(state => ({}), {getMovies, cleanSearch})(SearchField)