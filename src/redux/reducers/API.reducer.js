import * as Actions from '../actions'

export default (state = {}, action) => {
    switch (action.type) {
        case Actions.GET_MOVIES:
        {
            const {payload, page, term, append} = action

            if(append){
                const {result: {Search}} = state
                const result = Search.concat(payload.Search)
                return Object.assign({}, state, {result:{...payload, Search: result}, page, term})
            }

            return Object.assign({}, state, {result:{...payload}, page, term})
        }
        case Actions.GET_MOVIE:
        {
            const {payload, id} = action

            return Object.assign({}, state, {detailData:{...payload}, id})
        }
        case Actions.CLEAN_SEARCH:
            return Object.assign({}, state, {result:{Search:null, Response: 'True'}})

        default:
            return state
    }
}