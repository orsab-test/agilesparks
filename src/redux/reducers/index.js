import {combineReducers} from 'redux';
import API from './API.reducer'

const rootReducer = combineReducers({
    API
});

export default rootReducer;