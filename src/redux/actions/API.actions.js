import {keys} from '../../config'

export const GET_MOVIES = 'GET_MOVIES'
export const GET_MOVIE = 'GET_MOVIE'
export const CLEAN_SEARCH = 'CLEAN_SEARCH'

const getMovies = ({term, page = 1, type = 'movie', append = false}) => {
    return async (dispatch) => {
        const payload = await _get({s: term, page, type})

        dispatch({
            type: GET_MOVIES,
            payload,
            term,
            append,
            page
        })
    }
}

const getMovie = ({id}) => {
    return async (dispatch) => {
        const payload = await _get({i:id})

        dispatch({
            type: GET_MOVIE,
            payload,
            id
        })
    }
}

const cleanSearch = () => {
    return {
        type: CLEAN_SEARCH,
    }
}

const _get = async (variables) => {
    variables.apikey = keys.API_KEY
    const params = Object.keys(variables).map(key => `${key}=${encodeURIComponent(variables[key])}`).join('&')

    const response = await fetch(`${keys.API_ENDPOINT}/?${params}`,{
        method: 'GET',
        headers: {
            "Content-Type": "application/json",
        },
    })
    const json = await response.json()
    return json
}

export {
    getMovie,
    getMovies,
    cleanSearch
}